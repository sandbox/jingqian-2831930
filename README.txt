Ubercart Patron

Ubercart Patron is a set of modules designed to allow you:

1) Ubercart Product field
--------------------------------------------------------------------------------
Having product field and payment field available in entity form.(Those fields
are based on field API which potentially could be used in any field API based
entity structure eg: node field with some code base change)

 A)Product field allowed user not only add in product as a field into entity
   form with selected display mod but also allowed to bind recurring payment
   rules into individual product field

 B)Payment field allowed user attach ubercart store enabled payment method to the
   entity form. It allowed user mapping the existing entity form fields to the
   ubercart checkout address fields so the entity form generated order will having
   right address info

2) Ubercart short cut payment system
--------------------------------------------------------------------------------
Allowed ubercart purchase action on entity form redirect to payment gateway directly
(DPS support for now) instead of passing through normal ubercart checkout process

3) uc_dps
--------------------------------------------------------------------------------
DPS payment with pxpay2 support tune based on https://www.drupal.org/project/uc_paymentexpress

4) Recurring payment
--------------------------------------------------------------------------------
Recurring payment support and management for any orders having
recurring_payment_time_unit and recurring_payment_time_frequency value in it’s
product data section


How to use:

Example 1: Set up donate payment form
--------------------------------------------------------------------------------
(You may want to eable Entityforms Block to suppose block based purchase form)

1)Enable all the modules

2)Go to admin/store/orders/ubercart-product-field-config to generate a demo donate
  attribute

3)Config ubercart store admin/store/settings/store and enable DPS payment
  admin/store/settings/payment
  Notice: you need to get the DPS URL and PX Access User Id from DPS
  (https://www.paymentexpress.com). If recurring payment also required then please
  also ask DPS for your PX POST Access url, PX POST Access User Id and PX POST
  Access Key

4)Add “Product” node, name it donate. Disable the shippable check box. Add generated
  attributes from step 2). Enable / tune related options from “Options” tab. Edit
  adjustments with right sku code via click “Adjustments” tab.

5)Go to admin/structure/entityform_types add a new entityform type. Set up roles
  permission based on your needs. Click “Entity block settings” to config form as a
  block.

6)Add in “Ubercart Donate Product” field. Choose created donate product (created at
  step 4) from “Allowed Product ” field. “Allowed Display” leave as default if there
  is no special display wanted. Uncheck “Allowed none option” if donate selection is
  compulsory field. Check “Product allowed recurring payment?” check box will give you
  more options. Notice that Payment unit has to be a int value and at least one frequency
  value has to be selected.

7)Add in email field and make it required field. (https://www.drupal.org/project/email
  recommend)

8)Add in “Ubercart Payment” field. Chose “DPS” from “Allowed Payments”. Choose added
  email field for “Field from the entity form that used to generate a new user if none
  login and no matched mail address” options. Do the mapping between created entity
  form fields and ubercart address fields. Please leave option value untouched if
  none required ubercart field related entity form field is not exist.
  *“Ubercart Payment” field need to be created at very last step of the entity form.

9)If donate form need to be rendered as a block then please create a node and assign
  the entity form block to that node path.

10)Use admin/store/orders/recurring-payment-config to config how many recurring orders
   can be processed in each cron

11)Use admin/store/orders/remove-recurring-payment to manage the recurring payment orders


Some other examples:
--------------------------------------------------------------------------------
1)“Ubercart Product - Without Attributes” field: Fill in all required value.
  “Allowed Qty Options” will allowed user choose quantity of product from purchase
  form. Set up quantity select field is same as standard drupal selection field.
  For example: 0|None or 1|1.

2)Ubercart Product: Same as 1) without quantity setting.

3)Ubercart Dynamic Product - Without Attributes: Allowed user set up dynamic product
  field on which product loaded via pre defined logic. For now it is based on
  current entity form attached node (more logic and hook on development and for
  developer use only)

4)“Assign node info to product” option allowed user define int position which used
	as pass in var in arg() to load related node. The loaded node info then saved
	into the product data section of the purchased order and can be checked / displayed
	when load  the order later on. This is normally used for node view / publish
	permission purchase. For example, created a product “node publish for 30 days for
	$50 “ and assign it to a node purchase form (developer use only function and more
	easy config hook and function will come later)

