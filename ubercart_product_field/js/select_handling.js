(function($) {
	function enable_unckeck_for_radio_v2(field_name) {
		$(field_name).mousedown(function(e){
			var $self = $(this);
      if($self.find('input[type="radio"]').is(':checked')){
        var uncheck = function(){
          setTimeout(function(){
          	$self.find('input[type="radio"]').removeAttr('checked');
          },0);
        };
        var unbind = function(){
          $self.unbind('mouseup',up);
        };
        var up = function(){
          uncheck();
          unbind();
        };
        $self.bind('mouseup',up);
        $self.one('mouseout', unbind);
      }
    });
	}
	
	Drupal.behaviors.select_handling = {
	  attach: function (context, settings) {
	  	enable_unckeck_for_radio_v2('div.donate-free-radio > div');
	  }
	};
})(jQuery);