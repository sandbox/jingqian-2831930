<?php

/**
 * @file
 * Define product field config form.
 */

/**
 * Implementation of hook_config_form.
 */
function _ubercart_product_field_config_form() {
  $form = array();

  $form['ubercart_product_field_active_uc_attributes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Generate Donate Attribute'),
    '#default_value' => variable_get('ubercart_product_field_active_uc_attributes', 0),
  );

  $form['ubercart_product_field_attribute_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Attribute name'),
    '#size' => 30,
    '#maxlength' => 60,
    '#default_value' => variable_get('ubercart_product_field_attribute_name', 'Donate'),
  );

  $form['generate_donate_attribute'] = array(
    '#markup' => '<a href="/admin/store/orders/generate-donate-attribute" target="_blank">Generate Donate Attribute (you need to click "Save configuration" first)</a></br>',
  );

  return system_settings_form($form);
}
