<?php

/**
 * @file
 * Bypass the standard ubercart checkout process and directlly send user to the payment gateway.
 */

/**
 * Init order without pass checkout process.
 */
function ubercart_short_cut_init_order($address, $email = FALSE, $delivery_address = FALSE, $payment_method = 'dps', $order_status = 'in_checkout') {
  global $user;
  if ($user->uid < 1 && $email === FALSE) {
    return FALSE;
  }

  $new_order = uc_order_new($user->uid);
  $new_order->primary_email = FALSE;
  $new_order->billing_first_name = FALSE;
  $new_order->billing_last_name = FALSE;
  $new_order->billing_phone = '';

  $new_order->delivery_first_name = FALSE;
  $new_order->delivery_last_name = FALSE;
  $new_order->delivery_phone = '';

  if ($user->uid < 1 && $email) {
    $new_order->primary_email = $email;
  }
  else {
    $new_order->primary_email = $user->mail;
    $temp_user = user_load($user->uid);
    if (isset($temp_user->field_first_name[LANGUAGE_NONE][0]['value']) && strlen(trim($temp_user->field_first_name[LANGUAGE_NONE][0]['value']))) {
      $new_order->billing_first_name = $temp_user->field_first_name[LANGUAGE_NONE][0]['value'];
    }
    if (isset($temp_user->field_last_name[LANGUAGE_NONE][0]['value']) && strlen(trim($temp_user->field_last_name[LANGUAGE_NONE][0]['value']))) {
      $new_order->billing_last_name = $temp_user->field_last_name[LANGUAGE_NONE][0]['value'];
    }
    if (isset($temp_user->field_phone[LANGUAGE_NONE][0]['value']) && strlen(trim($temp_user->field_phone[LANGUAGE_NONE][0]['value']))) {
      $new_order->billing_phone = $temp_user->field_phone[LANGUAGE_NONE][0]['value'];
    }
  }
  // Address maping can be much easier via using ubercart_product_field solution!!!!! need to change below later on.
  if (isset($address['first_name']) && strlen(trim($address['first_name']))) {
    $new_order->billing_first_name = $address['first_name'];
  }

  if (isset($address['last_name']) && strlen(trim($address['last_name']))) {
    $new_order->billing_last_name = $address['last_name'];
  }

  if (isset($address['phone']) && strlen(trim($address['phone']))) {
    $new_order->billing_phone = $address['phone'];
  }

  if (isset($address['street']) && strlen(trim($address['street']))) {
    $new_order->billing_street1 = $address['street'];
  }

  if (isset($address['street1']) && strlen(trim($address['street1']))) {
    $new_order->billing_street1 = $address['street1'];
  }

  if (isset($address['postal_code']) && strlen(trim($address['postal_code']))) {
    $new_order->billing_postal_code = $address['postal_code'];
  }

  if (isset($address['suburb']) && strlen(trim($address['suburb']))) {
    $new_order->billing_zone = $address['suburb'];
  }

  if (isset($address['city']) && strlen(trim($address['city']))) {
    $new_order->billing_city = $address['city'];
  }

  if (isset($address['country']) && strlen(trim($address['country']))) {
    $new_order->billing_country = $address['country'];
  }

  if (is_array($delivery_address)) {
    if (isset($delivery_address['first_name']) && strlen(trim($delivery_address['first_name']))) {
      $new_order->delivery_first_name = $delivery_address['first_name'];
    }

    if (isset($delivery_address['last_name']) && strlen(trim($delivery_address['last_name']))) {
      $new_order->delivery_last_name = $delivery_address['last_name'];
    }

    if (isset($delivery_address['phone']) && strlen(trim($delivery_address['phone']))) {
      $new_order->delivery_phone = $delivery_address['phone'];
    }

    if (isset($delivery_address['street']) && strlen(trim($delivery_address['street']))) {
      $new_order->delivery_street1 = $delivery_address['street'];
    }

    if (isset($delivery_address['suburb']) && strlen(trim($delivery_address['suburb']))) {
      $new_order->delivery_zone = $delivery_address['suburb'];
    }

    if (isset($delivery_address['city']) && strlen(trim($delivery_address['city']))) {
      $new_order->delivery_city = $delivery_address['city'];
    }

    if (isset($delivery_address['country']) && strlen(trim($delivery_address['country']))) {
      $new_order->delivery_country = $delivery_address['country'];
    }

    if (isset($delivery_address['postal_code']) && strlen(trim($delivery_address['postal_code']))) {
      $new_order->delivery_postal_code = $delivery_address['postal_code'];
    }
  }

  $new_order->currency = 'NZD';
  $new_order->created = time();
  $new_order->order_status = $order_status;

  $new_order->payment_method = $payment_method;

  uc_order_save($new_order);

  return uc_order_load($new_order->order_id);
}

/**
 * Add products into the given order.
 */
function ubercart_short_cut_add_product($order, $product_id, $price = FALSE, $qty = 1, $data = array('attributes' => array()), $attributes = array()) {
  $product = uc_product_load_variant($product_id, $data);
  if ($product) {
    if ($price) {
      $product->price = number_format($price, 2, '.', '');
    }
    else {
      $product->price = number_format($product->sell_price, 2, '.', '');
    }
    $product->qty = $qty;

    foreach ($attributes as $key => $value) {
      $product->$key = $value;
    }

    if (isset($order->products) && is_array($order->products)) {
      $order->products[] = $product;
    }
    else {
      $order->products = array($product);
    }
    uc_order_save($order);
    return uc_order_load($order->order_id);
  }
  return FALSE;
}

/**
 * Send created order to DPS payment gateway.
 */
function ubercart_short_cut_send_to_dps($order) {
  if (isset($_SESSION['cart_order'])) {
    unset($_SESSION['cart_order']);
  }
  $_SESSION['cart_order'] = $order->order_id;

  if (isset($_SESSION['uc_checkout'][$order->order_id])) {
    unset($_SESSION['uc_checkout'][$order->order_id]);
  }
  $_SESSION['uc_checkout'][$order->order_id]['do_complete'] = TRUE;

  $order_id = intval($_SESSION['cart_order']);
  $send_order = uc_order_load($order_id);
  $send_order->order_total = number_format($send_order->order_total, 2, '.', '');
  uc_dps_goto($send_order);
}

/**
 * Assign attributes into products.
 */
function prepare_product_package($selected_option_id, $product_id, $attribute_id) {
  $data = FALSE;
  $attributes = FALSE;
  $option_price = FALSE;
  $attribute_name = FALSE;

  $attribute_info_query = db_query("SELECT name
                                    FROM {uc_attributes}
                                    WHERE aid = :aid
                                    LIMIT 1", array(':aid' => $attribute_id));
  $attribute_info = $attribute_info_query->fetchObject();
  if ($attribute_info) {
    $attribute_name = $attribute_info->name;
  }

  $attribute_query = db_query("SELECT combination, model
                               FROM uc_product_adjustments
                               WHERE nid = :nid", array(':nid' => $product_id));
  foreach ($attribute_query as $attribute) {
    $attributes_info = unserialize($attribute->combination);
    if (isset($attributes_info[$attribute_id])) {
      if ($selected_option_id == $attributes_info[$attribute_id]) {
        $option_model = $attribute->model;
        $option_name = FALSE;
        $attribute_detail_query = db_query("SELECT price
                                            FROM {uc_product_options}
                                            WHERE nid = :nid
                                            AND oid = :oid
                                            LIMIT 1", array(':nid' => $product_id, ':oid' => $selected_option_id));
        $temp_price = $attribute_detail_query->fetchObject();
        if ($temp_price) {
          $option_price = number_format($temp_price->price, 2, '.', '');
        }

        $attribute_detail_query = db_query("SELECT name
                                            FROM {uc_attribute_options}
                                            WHERE aid = :aid AND oid = :oid LIMIT 1", array(':aid' => $attribute_id, ':oid' => $selected_option_id));
        $temp_name = $attribute_detail_query->fetchObject();
        if ($temp_name) {
          $option_name = t($temp_name->name);
        }

        if ($option_model && $option_price && $option_name) {
          $data = array('attributes' => array($attribute_name => array($selected_option_id => $option_name)));
          $attributes = array('model' => $option_model, 'price' => $option_price);
        }
        break;
      }
    }
  }

  if ($data && $attributes && $option_price) {
    return array(
      'data' => $data,
      'attributes' => $attributes,
      'price' => $option_price,
    );
  }

  return FALSE;
}

/**
 * Assign right options and price to product.
 */
function prepare_product_package_by_model($product_id, $option_model) {
  $data = array('attributes' => array());
  $attributes = FALSE;
  $option_price = 0;
  $attribute_name = FALSE;

  $selected_model_options = locate_product_options_by_model($product_id, $option_model);

  foreach ($selected_model_options as $attribute_id => $option_id) {
    $attribute_info_query = db_query("SELECT name
                                      FROM {uc_attributes}
                                      WHERE aid = :aid
                                      LIMIT 1", array(':aid' => $attribute_id));
    $attribute_info = $attribute_info_query->fetchObject();
    if ($attribute_info) {
      $attribute_name = $attribute_info->name;
      $option_name = FALSE;

      $attribute_detail_query = db_query("SELECT price
                                          FROM {uc_product_options}
                                          WHERE nid = :nid
                                          AND oid = :oid
                                          LIMIT 1", array(':nid' => $product_id, ':oid' => $option_id));
      $temp_price = $attribute_detail_query->fetchObject();
      if ($temp_price) {
        $option_price = $option_price + $temp_price->price;
      }

      $attribute_detail_query = db_query("SELECT name
                                          FROM {uc_attribute_options}
                                          WHERE aid = :aid AND oid = :oid LIMIT 1", array(':aid' => $attribute_id, ':oid' => $option_id));
      $temp_name = $attribute_detail_query->fetchObject();
      if ($temp_name) {
        $option_name = t($temp_name->name);
      }

      $data['attributes'][$attribute_name] = array($option_id => $option_name);
    }

  }

  if ($option_price) {
    $option_price = number_format($option_price, 2, '.', '');
    $attributes = array('model' => $option_model, 'price' => $option_price);
  }
  elseif ($option_price == 0) {
    $attributes = array('model' => $option_model, 'price' => 0);
  }

  if ($data && $attributes && ($option_price || $option_price == 0)) {
    return array(
      'data' => $data,
      'attributes' => $attributes,
      'price' => $option_price,
    );
  }

  return FALSE;
}

/**
 * Dynamic price for donate free input price.
 */
function prepare_product_package_by_model_free_price($product_id, $option_model, $price) {
  $data = array('attributes' => array());
  $attributes = FALSE;
  $option_price = $price;
  $attribute_name = FALSE;

  $selected_model_options = locate_product_options_by_model($product_id, $option_model);

  foreach ($selected_model_options as $attribute_id => $option_id) {
    $attribute_info_query = db_query("SELECT name
                                      FROM {uc_attributes}
                                      WHERE aid = :aid
                                      LIMIT 1", array(':aid' => $attribute_id));
    $attribute_info = $attribute_info_query->fetchObject();
    if ($attribute_info) {
      $attribute_name = $attribute_info->name;
      $option_name = FALSE;

      $attribute_detail_query = db_query("SELECT name
                                          FROM {uc_attribute_options}
                                          WHERE aid = :aid AND oid = :oid LIMIT 1", array(':aid' => $attribute_id, ':oid' => $option_id));
      $temp_name = $attribute_detail_query->fetchObject();
      if ($temp_name) {
        $option_name = t($temp_name->name);
      }

      $data['attributes'][$attribute_name] = array($option_id => $option_name);
    }

  }

  $option_price = number_format($option_price, 2, '.', '');
  $attributes = array('model' => $option_model, 'price' => $option_price);

  if ($data && $attributes && ($option_price || $option_price == 0)) {
    return array(
      'data' => $data,
      'attributes' => $attributes,
      'price' => $option_price,
    );
  }

  return FALSE;
}

/**
 * Assign product with right qty.
 */
function prepare_product_package_by_qty($product_id, $qty) {
  $data = array('attributes' => array());
  $attributes = FALSE;
  $temp_product = node_load($product_id);
  if ($temp_product && $temp_product->type == 'product') {
    $temp_price = number_format($temp_product->sell_price, 2, '.', '');
    $attributes = array('model' => $temp_product->model, 'price' => $temp_price);
    return array(
      'data' => $data,
      'attributes' => $attributes,
      'price' => $temp_price,
      'qty' => $qty,
    );
  }
  return FALSE;
}

/**
 * Return Options based on give model.
 */
function locate_product_options_by_model($product_id, $product_model) {
  $attribute_query = db_query("SELECT combination
                               FROM uc_product_adjustments
                               WHERE nid = :nid
                               AND model = :model", array(':nid' => $product_id, ':model' => $product_model));
  $attribute_info = $attribute_query->fetchObject();
  if ($attribute_info) {
    $temp_info = unserialize($attribute_info->combination);
    if (is_array($temp_info)) {
      return $temp_info;
    }
  }
  return FALSE;
}

/**
 * Clone old order into new order.
 */
function ubercart_short_cut_clone_order(&$new_order, $old_order) {
  // Need change to match the dynamic address field.
  $new_order->payment_method = $old_order->payment_method;
  $new_order->currency = $old_order->currency;

  $enabled_checkout_fields = variable_get('uc_address_fields');
  // We may want to log this order due to missing order required fields later on.
  /*
  $required_ubercart_fields = variable_get('uc_address_fields_required');
  $error = array();
  */
  if ($enabled_checkout_fields) {
    foreach ($enabled_checkout_fields as $field_base_name => $detla) {
      // Holding for required field checking: $var_set = FALSE; ?
      if (isset($old_order->{'billing_' . $field_base_name}) &&
          strlen($old_order->{'billing_' . $field_base_name}) &&
          isset($new_order->{'billing_' . $field_base_name})) {
        $new_order->{'billing_' . $field_base_name} = $old_order->{'billing_' . $field_base_name};
        // Holding for required field checking: $var_set = TRUE; ?
      }
      if (isset($old_order->{'delivery_' . $field_base_name}) &&
          strlen($old_order->{'delivery_' . $field_base_name}) &&
          isset($new_order->{'delivery_' . $field_base_name})) {
        $new_order->{'delivery_' . $field_base_name} = $old_order->{'delivery_' . $field_base_name};
        // Holding for required field checking: $var_set = TRUE; ?
      }

      /*
      if (isset($required_ubercart_fields[$field_base_name]) && $var_set === FALSE) {
        $error['required'] = "Required Field is not set";
				//we may want to log this order due to missing order required fields later on
      }
      */
    }

    if (module_exists('uc_extra_fields_pane')) {
      $old_extra_billing_value = array();
      if (isset($old_order->uc_addresses['billing'])) {
        $value_list = uc_extra_fields_pane_value_list_load($old_order->order_id, UCXF_Value::UCXF_VALUE_ORDER_BILLING);
        foreach ($value_list as $oValue) {
          $old_extra_billing_value[$oValue->db_name] = $oValue->value;
        }
      }

      $old_extra_shipping_value = array();
      if (isset($old_order->uc_addresses['shipping'])) {
        $value_list = uc_extra_fields_pane_value_list_load($old_order->order_id, UCXF_Value::UCXF_VALUE_ORDER_DELIVERY);
        foreach ($value_list as $oValue) {
          $old_extra_shipping_value[$oValue->db_name] = $oValue->value;
        }
      }

      if (count($old_extra_billing_value)) {
        $aElementTypes = array(
          'billing' => array(
            'pane_type' => 'extra_billing',
            'element_type' => UCXF_Value::UCXF_VALUE_ORDER_BILLING,
            'prefix' => 'billing_',
            'uc_addresses_type' => 'billing',
          ),
        );

        foreach ($aElementTypes as $data) {
          $fields = UCXF_FieldList::getFieldsFromPane($data['pane_type']);
          foreach ($fields as $fieldname => $field) {
            if (isset($old_extra_billing_value[$fieldname]) && strlen(trim($old_extra_billing_value[$fieldname]))) {
              try {
                uc_extra_fields_pane_value_save(
                  array(
                    'element_id' => $new_order->order_id,
                    'element_type' => $data['element_type'],
                    'field_id' => $field->field_id,
                    'value' => $old_extra_billing_value[$fieldname],
                  )
                );
              }
              catch (UcAddressesException $e) {
                watchdog('ucxf', 'An exception occurred when trying to save value for field %field when editing order @order_id: @exception', array(
                  '%field' => $fieldname,
                  '@order_id' => $new_order->order_id,
                  '@exception' => $e->getMessage(),
                ), WATCHDOG_WARNING);
              }
            }
          }
        }
      }

      if (count($old_extra_shipping_value)) {
        $aElementTypes = array(
          'delivery' => array(
            'pane_type' => 'extra_delivery',
            'element_type' => UCXF_Value::UCXF_VALUE_ORDER_DELIVERY,
            'prefix' => 'delivery_',
            'uc_addresses_type' => 'shipping',
          ),
        );

        foreach ($aElementTypes as $data) {
          $fields = UCXF_FieldList::getFieldsFromPane($data['pane_type']);
          foreach ($fields as $fieldname => $field) {
            if (isset($old_extra_shipping_value[$fieldname]) && strlen(trim($old_extra_shipping_value[$fieldname]))) {
              try {
                uc_extra_fields_pane_value_save(
                  array(
                    'element_id' => $new_order->order_id,
                    'element_type' => $data['element_type'],
                    'field_id' => $field->field_id,
                    'value' => $old_extra_shipping_value[$fieldname],
                  )
                );
              }
              catch (UcAddressesException $e) {
                watchdog('ucxf', 'An exception occurred when trying to save value for field %field when editing order @order_id: @exception', array(
                  '%field' => $fieldname,
                  '@order_id' => $new_order->order_id,
                  '@exception' => $e->getMessage(),
                ), WATCHDOG_WARNING);
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Locate right price alter based on given model.
 */
function ubercart_short_cut_model_load($product, $model) {
  $price_alter = 0;
  $attribute_info_query = db_query("SELECT combination
                                    FROM {uc_product_adjustments}
                                    WHERE model = :model
                                    AND nid = :nid
                                    LIMIT 1", array(':model' => $model, ':nid' => $product->nid));
  $attribute_info = $attribute_info_query->fetchObject();
  if ($attribute_info) {
    $attributes_info = unserialize($attribute_info->combination);
    foreach ($attributes_info as $temp_option_id) {
      $attribute_detail_query = db_query("SELECT price
                                          FROM {uc_product_options}
                                          WHERE nid = :nid
                                          AND oid = :oid
                                          LIMIT 1", array(':nid' => $product->nid, ':oid' => $temp_option_id));
      $temp_price = $attribute_detail_query->fetchObject();
      if ($temp_price) {
        $price_alter += number_format($temp_price->price, 2, '.', '');
      }
    }
  }
  return $price_alter;
}
