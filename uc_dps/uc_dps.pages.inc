<?php

/**
 * @file
 * DPS menu items.
 */

/**
 * Finalizes DPS transaction.
 */
 
function uc_dps_complete($cart_id = 0) {
  if (isset($_REQUEST["result"])) {
    module_load_include('inc', 'uc_dps', 'pxpay_curl');    

    if(variable_get('uc_dps_live_mode', FALSE)){
      $pxpay = new PxPay_Curl(variable_get('uc_dps_live_url', ''), variable_get('uc_dps_live_user_id', ''), variable_get('uc_dps_live_key', ''));
    } 
    else {
      $pxpay = new PxPay_Curl(variable_get('uc_dps_test_url', ''), variable_get('uc_dps_test_user_id', ''), variable_get('uc_dps_test_key', ''));
    }
    
    $enc_hex = $_REQUEST["result"];
    $rsp = $pxpay->getResponse($enc_hex);
    $result = new stdClass();
    $result->success           = $rsp->getSuccess();         // =1 when request succeeds
    #$result->retry             = $rsp->getRetry();           // =1 when a retry might help
    #$result->statusrequired    = $rsp->getStatusRequired();  // =1 when transaction "lost"
    $result->amountsettlement  = $rsp->getAmountSettlement();    
    $result->authcode          = $rsp->getAuthCode();        // from bank
    $result->cardname          = $rsp->getCardName();        // e.g. "Visa"
    $result->dpstxnref         = $rsp->getDpsTxnRef();

    // the following values are returned, but are from the original request
    $result->txntype           = $rsp->getTxnType();
    $result->txndata1          = $rsp->getTxnData1();
    $result->txndata2          = $rsp->getTxnData2();
    $result->txndata3          = $rsp->getTxnData3();
    $result->currencyinput     = $rsp->getCurrencyInput();
    $result->emailaddress      = $rsp->getEmailAddress();
    $result->txnid             = $rsp->getMerchantReference();
    $result->dpsbillingid      = $rsp->getDpsBillingId();
    $result->dateexpiry        = $rsp->getDateExpiry();
    
    $order = uc_order_load($result->txnid);         

    watchdog('uc_dps', 'Receiving new order notification for order !order_id.', array('!order_id' => $result->txnid));
    if ($result->success == "1") {
      //$comment = t('Order ID: @txn_id', array('@txn_id' => $order->order_id));
      $comment = t('DPS PXPay Auth code: @auth_code, Transaction ID: @txn_id', array(
        '@auth_code' => $result->authcode,
        '@txn_id'    => $result->dpstxnref,
      ));
      
      $payments = uc_payment_load_payments($order->order_id);
      
      // Check for duplicate transaction. DPS sends FPRN and also returns browser
      // with data to validate transaction, so we are likely to see both.
      //
      // Both notifications should set the payment for the order, but only once.
      //
      // The browser return should fire uc_cart_complete_sale() to log in if reqd.
      $duplicate_flag = FALSE;
      if (!empty($payments)) {
        foreach ($payments as $payment) {
          if ($payment->comment == $comment) {
            $duplicate_flag = TRUE;
          }
        }
      }
      
      if (!$duplicate_flag) {
        //$order->dpsbillingid = $result->dpsbillingid;
        //$order->dateexpiry = $result->dateexpiry;
        $order->data['dpsbillingid'] = $result->dpsbillingid;
        $order->data['dateexpiry'] = $result->dateexpiry;
        
        uc_order_save($order);
        
        if (isset($order->status) && $order->status == 'in_checkout') {
          uc_order_update_status($order->order_id, 'payment_received');
        }

        uc_payment_enter($order->order_id, 'uc_dps_pxpay_dps', $result->amountsettlement, $order->uid, $order->data['payment_details'], $comment);
        uc_order_comment_save($order->order_id, 0, t('DPS PXPay payment successful'), 'order', 'payment_received');
  
        uc_order_comment_save($order->order_id, 0, t('Payment of @amount submitted via DPS PXPay (@txn_id).', array(
          '@amount' => uc_currency_format($result->amountsettlement, FALSE),
          '@txn_id' => $order->data['payment_details']['pxpay_DpsTxnRef'],
        )), 'order', 'payment_received');
  
        uc_order_comment_save($order->order_id, 0, t('PXPay reported a payment of @amount.', array(
          '@amount' => uc_currency_format($result->amountsettlement, FALSE),
        )));
        
        watchdog('uc_dps', 'Transaction was completed!', array(), WATCHDOG_NOTICE);   
      }
      else {
        watchdog('uc_dps', 'Ignore duplicate dps triger!', array(), WATCHDOG_NOTICE);
      }  
        
      $_SESSION['cart_order'] = $order->order_id;
      $_SESSION['uc_checkout'][$_SESSION['cart_order']]['do_complete'] = TRUE;
    } else {
      //dsm($result);
      drupal_set_message(t('An error occurred while processing your order, please make sure the card detail is correct.'), 'error');
    }
  }
  
  drupal_goto('cart/checkout/complete');
}
