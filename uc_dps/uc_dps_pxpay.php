<?php
define( 'DPS_STATUS_REQUEST', 1 );
define( 'DPS_STATUS_RESPONSE', 2 );

Class siprod_dps_pxpay{

	public $sResponseUri;
	public $aFullResponse = Array();

	protected $nStatus; // 1 - REQUEST REPLY & 2 - RESPONSE
	protected $bResponseValid;
	protected $sXmlCurrentTag; //Stores current tag being processed by xml_parse


	public function siprod_dps_pxpay(){
	}


	public function doRequest( $dpsRequest ){
		$this->nStatus = DPS_STATUS_REQUEST;

		$this->processRequestResponse(
			$this->executePost(
				$this->createRequestXml( $dpsRequest )
			)
		);

		return $this->sResponseUri;
	}


	public function getResult( $result ){
		$this->nStatus = DPS_STATUS_RESPONSE;

		$this->processResult(
			$this->executePost(
				$this->createResultXml( $result )
			)
		);
		return $this->aFullResponse; //This array is manipulated from within processResult => parseXml
	}


	protected function createRequestXml( $dpsRequest ){
		$xml = '';
		$xml .= $this->createXMLElement( 'PxPayUserId', siprod_config::$dps['user'] );
		$xml .= $this->createXMLElement( 'PxPayKey', siprod_config::$dps['key'] );
		$xml .= $this->createXMLElement( 'AmountInput', sprintf( "%0.02f", $dpsRequest->nTotal ) );
		$xml .= $this->createXMLElement( 'CurrencyInput', 'NZD' );
		$xml .= $this->createXMLElement( 'MerchantReference', siprod_config::$dps['merchref'] );
		$xml .= $this->createXMLElement( 'EmailAddress', $dpsRequest->sEmail );
		$xml .= $this->createXMLElement( 'TxnData1', htmlentities( $dpsRequest->aData[0] ) );
		$xml .= $this->createXMLElement( 'TxnData2', htmlentities( $dpsRequest->aData[1] ) );
		$xml .= $this->createXMLElement( 'TxnData3', htmlentities( $dpsRequest->aData[2] ) );
		$xml .= $this->createXMLElement( 'TxnType', 'Purchase' );
		$xml .= $this->createXMLElement( 'TxnId', $dpsRequest->nTransactionId );
		$xml .= $this->createXMLElement( 'BillingId', '' );
		$xml .= $this->createXMLElement( 'EnableAddBillCard', 0 );
		$xml .= $this->createXMLElement( 'UrlFail', siprod_config::$dps['urlfailure'] );
		$xml .= $this->createXMLElement( 'UrlSuccess', siprod_config::$dps['urlsuccess'] );
		$xml = $this->createXMLElement( 'GenerateRequest', "\n$xml" );
		return $xml;
	}


	protected function createResultXml( $result ){
		$xml = '';
		$xml .= $this->createXMLElement( 'PxPayUserId', siprod_config::$dps['user'] );
		$xml .= $this->createXMLElement( 'PxPayKey', siprod_config::$dps['key'] );
		$xml .= $this->createXMLElement( 'Response', $result );
		$xml = $this->createXMLElement( 'ProcessResponse', "\n$xml" );
		return $xml;
	}


	protected function executePost( $xml ){
		$message =
		"POST /pxpay/pxaccess.aspx HTTP/1.1\n".
		"Host: www.paymentexpress.com\n".
		"Content-type: application/x-www-form-urlencoded\n".
		"Content-length: ".strlen($xml)."\n".
		"Accept: */*\n".
		"Connection: close\n\n".
		$xml."\n\n";

		$fp = fsockopen( 'ssl://www.paymentexpress.com', 443, $errstr, $errnum, 30 ) or die( "$errnum $errstr" );
		fwrite( $fp, $message );
		fflush( $fp );

		$result = '';
		while ( !feof( $fp ) ) {
			$result .= @fgets( $fp, 128 );
		}
		fclose( $fp );

		return $result;
	}


	protected function processRequestResponse( $result ){
		$start = strpos( $result, '<Request' );
		$end = strpos( $result, '</Request>' );
		$response = substr( $result, $start, $end-$start+10 );
		$this->parseXml( $response );
	}


	protected function processResult( $result ){
		$start = strpos($result,'<Response');
		$end = strpos($result,'</Response>');
		$response = substr( $result, $start, $end-$start+11 );
		$this->parseXml( $response );
	}


	protected function parseXml( $response ){
		$xml_parser = xml_parser_create();
		xml_set_object( $xml_parser, $this );
		xml_set_element_handler( $xml_parser, 'xmlStartElement', 'xmlEndElement' );
		xml_set_character_data_handler( $xml_parser, 'xmlCharacterData' );
		xml_parse( $xml_parser, $response, 1 );
		//The xml handling functions directly manipulate this object's members.
		//Therefore no return values are needed.
	}


	/* XML FUNCTIONS */
	function createXMLElement($elementName,$content,$attributes=array()) { // encoding of content and attribute values is assumed to have happened prior to this function call
		reset($attributes);
		$attributeStr='';
		foreach ($attributes as $name => $value) {
			$attributeStr .= " $name=\"$value\"";
		}
		return "<$elementName$attributeStr>$content</$elementName>\n"; //BUG
	}


	function xmlStartElement( $parser, $tagName, $attrs ){
		$this->sXmlCurrentTag = $tagName;
		if ( $this->nStatus == DPS_STATUS_REQUEST ) {
			if ( $tagName == 'REQUEST' ) {
				$this->bResponseValid = $attrs['VALID'];
			}
		} else if ($this->nStatus == DPS_STATUS_RESPONSE ) {
			if ( $tagName == 'RESPONSE' ) {
				$this->aFullResponse['VALID'] = $attrs['VALID'];
			}
		}
	}


	function xmlEndElement( $parser, $tagName ){
	}


	function xmlCharacterData( $parser, $data ){
		if ( $this->nStatus == DPS_STATUS_REQUEST ) {
			if ( $this->sXmlCurrentTag == 'URI' ) {
				$this->sResponseUri .= $data;
			}
		} else if ( $this->nStatus == DPS_STATUS_RESPONSE ) {
			$this->aFullResponse[ $this->sXmlCurrentTag ] .= $data;
		}
	}

}