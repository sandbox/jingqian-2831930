<?php
Class siprod_dps_request{

	public $nTransactionId;
	public $nTotal;
	public $aData; //Can support up to 3 strings of additional data
	public $sEmail = '';

	public function siprod_dps_request( $nTid, $nTotal, $aData ){
		$this->nTransactionId = $nTid;
		$this->nTotal = $nTotal;
		$this->aData = $aData;
	}

}