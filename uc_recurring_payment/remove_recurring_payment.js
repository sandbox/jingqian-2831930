(function ($) {
	
	Drupal.behaviors.remove_recurring_payment = {
  	attach: function (context, settings) {
  		
			$('.remove-order-payment').live( "click", function() {
				var confirmed = confirm('This action will remove the recurring payment permanently!');
		    if (confirmed) {
		    	var $row = $(this).closest("tr");
		    	var order_id = $(this).attr('order_ref');
					var base_url = Drupal.settings.remove_recurring_payment_path;
					var dataset = {'order_id': order_id};
					$.getJSON(
			      base_url,
			      dataset,
			      function(resp){
			      	if(resp.processed){
			      		$row.remove();
			      	}
			      }
			    );
			    
		    }
				return false;
			});

  }}

})(jQuery);
