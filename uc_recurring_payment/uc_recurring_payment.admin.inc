<?php

/**
 * @file
 * Implementation of uc_recurring_payment_menu config vars.
 */

/**
 * Implementation of _uc_recurring_payment_config_form for vars config.
 */
function _uc_recurring_payment_config_form() {
  $form = array();

  $form['uc_recurring_payment_item_per_cron'] = array(
    '#type' => 'textfield',
    '#title' => t('Items per cron to process'),
    '#size' => 30,
    '#maxlength' => 60,
    '#default_value' => variable_get('uc_recurring_payment_item_per_cron', 100),
  );

  return system_settings_form($form);
}
