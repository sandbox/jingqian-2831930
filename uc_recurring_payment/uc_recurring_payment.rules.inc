<?php

/**
 * @file
 * Bring in recurring payment rule.
 */

/**
 * Implements hook_rules_condition_info().
 */
function uc_recurring_payment_rules_condition_info() {
  $conditions['uc_order_has_recurring_payment'] = array(
    'label' => t("Check an order's product has recurring payment"),
    'group' => t('Order'),
    'parameter' => array(
      'order' => array(
        'type' => 'uc_order',
        'label' => t('Order'),
      ),
    ),
  );
  return $conditions;
}

/**
 * Implements customized condition.
 */
function uc_order_has_recurring_payment($order) {
  if (isset($order->products) && is_array($order->products)) {
    foreach ($order->products as $temp_product_obj) {
      if (isset($temp_product_obj->data['recurring_payment_time_unit']) && $temp_product_obj->data['recurring_payment_time_unit']
          && isset($temp_product_obj->data['recurring_payment_time_frequency']) && $temp_product_obj->data['recurring_payment_time_frequency']) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
 * Implements hook_rules_action_info.
 */
function uc_recurring_payment_rules_action_info() {
  $order_arg = array(
    'type' => 'uc_order',
    'label' => t('Order'),
  );

  $actions['recurring_payment_order_based'] = array(
    'label' => t('Setup recurring payment for current order'),
    'group' => t('Recurring payment set up'),
    'base' => 'recurring_payment_order_based',
    'parameter' => array(
      'order' => $order_arg,
    ),
    'module' => 'uc_recurring_payment',
  );

  return $actions;
}

/**
 * Implements action.
 */
function recurring_payment_order_based($order) {
  if (isset($order->products) && is_array($order->products)) {
    if (isset($order->data['made_by_recurring_payment']) && isset($order->data['base_order_id'])
        && $order->data['made_by_recurring_payment'] && $order->data['base_order_id']) {
      // Recurring payment from system.
      $base_recurring_payment_order = get_exist_recurring_payment_order_condition($order->data['base_order_id'], $order->uid, $order->data['dpsbillingid']);
      if ($base_recurring_payment_order) {
        // Set the next payment time.
        $previous_payment_due_time = $base_recurring_payment_order->last_payment_time;
        $next_payment_due = FALSE;
        // All the stored time should be already filtered with special requirements from the init set up and we should just need add on the new time.
        switch ($base_recurring_payment_order->time_frequency) {
          case 'month':
          case 'year':
          case 'week':
          case 'day':
            $next_payment_due = strtotime('+' . $base_recurring_payment_order->time_unit . ' ' . $base_recurring_payment_order->time_frequency, $previous_payment_due_time);
            break;
        }
        if ($next_payment_due) {
          db_update('uc_order_recurring_payment')
            ->fields(array(
              'last_payment_time' => $next_payment_due,
            ))
            ->condition('oid', $order->data['base_order_id'])
            ->condition('uid', $order->uid)
            ->condition('payment_key', $order->data['dpsbillingid'])
            ->execute();
        }
      }
    }
    else {
      // New payment with recurring payment set.
      $init_recurring_payment = FALSE;
      $recurring_payment_time_unit = FALSE;
      $recurring_payment_time_frequency = FALSE;

      foreach ($order->products as $temp_product_obj) {
        if (isset($temp_product_obj->data['recurring_payment_time_unit']) && $temp_product_obj->data['recurring_payment_time_unit']
            && isset($temp_product_obj->data['recurring_payment_time_frequency']) && $temp_product_obj->data['recurring_payment_time_frequency']) {
          $init_recurring_payment = TRUE;
          $recurring_payment_time_unit = $temp_product_obj->data['recurring_payment_time_unit'];
          $recurring_payment_time_frequency = $temp_product_obj->data['recurring_payment_time_frequency'];
          break;
        }
      }

      if ($init_recurring_payment && isset($order->data['dpsbillingid']) && $order->data['dpsbillingid']) {
        $existing_oid = get_exist_recurring_payment_order($order);
        if ($existing_oid === FALSE) {

          // Set the next payment time.
          $setup_time = time();
          $next_payment_due = FALSE;
          switch ($recurring_payment_time_frequency) {
            case 'month':
              // Need to make sure we do the 28 day check.
              $created_day = date('j', $setup_time);
              if ($created_day >= 28) {
                $created_day_formatted = date('M Y G:i', $setup_time);
                $tuned_day_formatted = '28 ' . $created_day_formatted;
                $tuned_day = strtotime($tuned_day_formatted);
                $next_payment_due = strtotime('+' . $recurring_payment_time_unit . ' ' . $recurring_payment_time_frequency, $tuned_day);
              }
              else {
                $next_payment_due = strtotime('+' . $recurring_payment_time_unit . ' ' . $recurring_payment_time_frequency, $setup_time);
              }
              break;

            case 'year':
            case 'week':
            case 'day':
              $next_payment_due = strtotime('+' . $recurring_payment_time_unit . ' ' . $recurring_payment_time_frequency, $setup_time);
              break;
          }

          if ($next_payment_due) {
            db_insert('uc_order_recurring_payment')
              ->fields(array(
                'oid' => $order->order_id,
                'uid' => $order->uid,
                'created' => time(),
                'time_unit' => $recurring_payment_time_unit,
                'time_frequency' => $recurring_payment_time_frequency,
                'last_payment_time' => $next_payment_due,
                'payment_key' => $order->data['dpsbillingid'],
              ))
              ->execute();
            foreach ($order->products as $temp_product_obj) {
              if (isset($temp_product_obj->data['recurring_payment_time_unit']) && $temp_product_obj->data['recurring_payment_time_unit']
                  && isset($temp_product_obj->data['recurring_payment_time_frequency']) && $temp_product_obj->data['recurring_payment_time_frequency']) {
                db_insert('uc_order_products_recurring_payment')
                  ->fields(array(
                    'oid' => $order->order_id,
                    'nid' => $temp_product_obj->nid,
                  ))
                  ->execute();
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Load existing recurring order via given oid, uid and payment key.
 */
function get_exist_recurring_payment_order_condition($oid, $uid, $payment_key) {
  $existing_recurring_payment = db_query("SELECT * FROM {uc_order_recurring_payment}
                                          WHERE oid = :oid
                                          AND uid = :uid
                                          AND payment_key = :payment_key
                                          LIMIT 1", array(
                                                      ':oid' => $oid,
                                                      ':uid' => $uid,
                                                      ':payment_key' => $payment_key,
                                                    )
                                        )->fetchObject();
  if ($existing_recurring_payment) {
    return $existing_recurring_payment;
  }
  return FALSE;
}

/**
 * Load existing recurring order via given order obj.
 */
function get_exist_recurring_payment_order($order) {

  $existing_recurring_payment = db_query("SELECT oid FROM {uc_order_recurring_payment}
                                          WHERE oid = :oid
                                          AND uid = :uid
                                          AND payment_key = :payment_key
                                          LIMIT 1", array(
                                                      ':oid' => $order->order_id,
                                                      ':uid' => $order->uid,
                                                      ':payment_key' => $order->data['dpsbillingid'],
                                                    )
                                        )->fetchObject();
  if ($existing_recurring_payment) {
    return $existing_recurring_payment->oid;
  }
  return FALSE;
}
