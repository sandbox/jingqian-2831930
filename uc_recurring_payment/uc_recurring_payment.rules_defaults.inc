<?php

/**
 * @file
 * Default Rules configurations.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uc_recurring_payment_default_rules_configuration() {
  $rule = rules_reaction_rule();
  $rule->label = t('Recurring payment');
  $rule->active = TRUE;
  $rule->event('uc_checkout_complete')
    ->condition('uc_order_has_recurring_payment', array(
      'order:select' => 'order',
    ))
    ->action('recurring_payment_order_based', array(
      'order:select' => 'order',
    ));

  $configs['uc_checkout_admin_notification'] = $rule;
  return $configs;
}
